# 深度学习多元时间序列预测

1. 时间序列预测（Time series forecasting），用历史观测值来预测时间序列在未来时间步上的值，可以粗略地分为两类：单步和多步预测 [Pan2020].  
1. 给定一个长时间序列 $\mathbf{X}^*$ 和一个有固定长度 $T$ 的回看窗口，基于过去 $T$ 步的历史值 $\mathbf{X}_{t-T+1 : t} = \{\mathbf{x}\_{t-T+1}, \ldots, \mathbf{x}\_{t} \}$，在 $t$ 时刻的 **单步预测值** 只有一个 $\hat{\mathbf{X}}\_{t+\tau : t+\tau} = \{\mathbf{x}\_{t+ \tau}\}$，而 **多步预测值** 有多个 $\hat{\mathbf{X}}\_{t+1 : t+\tau} = \{\mathbf{x}\_{t+1}, \ldots, \mathbf{x}\_{t+ \tau} \}$，其中 $\tau$ 是预测步长（horizon），$\mathbf{x}\_{t} \in \mathbb{R}^{d}$ 为序列在时间步 $t$ 的值，$d$ 是时间序列的条数.  
1. 为了简便，随后的讨论中我们忽略下标，用 $\mathbf{X}$ 和 $\hat{\mathbf{X}}$ 分别来表示历史数据和预测值.  
1. 对于多步预测，我们既可以直接用多步预测的目标（直接多步估计，direct multi-step (DMS) estimation），也能通过迭代使用一个单步模型得到多步的预测（迭代多步估计，iterated multi-step (IMS) estimation）. 但选择使用 DMS 或 IMS 方法时，需要权衡 “预测误差”、“估计方差”、“预测步长” 以及 “模型的非线性” [Shi2018]. 
1. 时间序列预测有很多方法，传统的自回归模型（autoregressive, AR）[Winters1960] 和 Holt-Winters [Holt2004] 是 ***需要 “专业知识” 和 “特征工程” 的参数模型***. 
1. 近来，深度学习方法大受欢迎，因为它们在序列数据建模中比起传统模型（如，ARIMA）有相当优越的表现. 

[Winters1960] Forecasting Sales by Exponentially Weighted Moving Averages.  
[Holt2004] Forecasting seasonals and trends by exponentially weighted moving averages.  
[Shi2018] Machine learning for spatiotemporal sequence forecasting: A survey.  
[Pan2020] Connecting the dots: Multivariate time series forecasting with graph neural networks. 

## 多步、多元、多尺度的方法

1. 区别于经典统计学习、机器学习的单步模型，神经网络可以构建 ***多输出的多步模型***。许多文献显示：与单步模型相比，多步模型表现优异；
1. 就像传统多元时间序列的 VAR 模型之于 AR 模型的推广，近年来有人用 “图神经网络” 对 ***序列间关系*** 进行建模
1. 特别地，多元模型可以结合无监督的聚类方法。先把多条时间序列分成几类，然后对每一类分别构建多元模型，以减低模型复杂度
1. 近来也有一些针对时间序列多尺度特征的神经网络模型，如 NBEATSx, TS2Vec, **SciNet+RevIN**

## 时序工具包
- 别人搜集整理的：[sktime - Related Software](https://www.sktime.org/en/latest/related_software.html)、[awesome_time_series_in_python](https://github.com/MaxBenChrist/awesome_time_series_in_python)（时序数据统一格式）
- 别人的一些见解：[一些时间序列处理工具包的简单比较（完事儿） - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/375883651)

大多工具包都在开发当中，我们参考 [darts 汇总的各种预测模型](https://unit8co.github.io/darts/README.html#forecasting-models)，并以此作为基线.  

## 深度学习时间序列的“同学们”

[[2004.10240] Neural forecasting: Introduction and literature overview](https://arxiv.org/abs/2004.10240) 
神经网络预测：导论与文献综述

[[2004.13408] Time Series Forecasting With Deep Learning: A Survey](https://arxiv.org/abs/2004.13408)
深度学习时间序列预测的综述、[译文](https://www.cnblogs.com/acboyty/p/14440686.html)

[[2202.07125] Transformers in Time Series: A Survey](https://arxiv.org/abs/2202.07125)
Transformers 时序综述

Github 仓库
1. 小项目
    - 基础教程[sklearn, tf1 keras]  
：https://github.com/fenago/deep-learning-time-series-forecasting
    - 实验综述[tf keras]  
：https://github.com/pedrolarben/TimeSeriesForecasting-DeepLearning
    - 个人研究0[stasmodels, sklearn, gluonts, tf keras]  
：https://github.com/jiwidi/time-series-forecasting-with-python  
多个模型在一个数据集上的测试对比
    - 个人研究1[tf keras]  
：https://github.com/LongxingTan/Time-series-prediction  
基础的深度学习模型
    - 个人研究2[torch]  
：https://github.com/EvilPsyCHo/Deep-Time-Series-Prediction  
几个特别的改进 seq2seq
    - 个人研究3[torch]  
：https://github.com/eth-sri/probabilistic-forecasts-attacks  
TCN 和 LSTM，有较详细的改进说明
1. 大项目
    - 基础的分类和回归模型[sktime, tf keras](under development)  
：https://github.com/sktime/sktime-dl
    - 最强的分类模型？MiniRocket[torch & fastai]  
：https://github.com/timeseriesAI/tsai
    - 亚马逊团队[mxnet]  
：https://github.com/awslabs/gluon-ts/tree/master/src/gluonts
    - 特别支持 transformer 模型[torch]  
：https://github.com/AIStream-Peelout/flow-forecast
1. 代码少，理论多
    - 微软团队的教程[tf keras]  
：https://github.com/Azure/DeepLearningForTimeSeriesForecasting  
[部分解读：预测的策略](https://zhuanlan.zhihu.com/p/321112816)
    - 论文汇总  
：https://github.com/Alro10/deep-learning-time-series
    - 前沿论文  
：https://github.com/fengyang95/Awesome-Deep-Learning-Based-Time-Series-Forecasting

博文
- 简单教程[torch]  
：https://curiousily.com/posts/time-series-forecasting-with-lstm-for-daily-coronavirus-cases/
- 使用深度学习进行时序预测(一元、单步) [matlab]（虽然不是 python，但训练细节仍值得参考）  
：https://ww2.mathworks.cn/help/deeplearning/ug/time-series-forecasting-using-deep-learning.html

微信公众号
- 人工智能学术前沿  [#时间序列预测](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUzNjk5Nzk1OA==&action=getalbum&album_id=1906875449712951300&scene=173&from_msgid=2247485963&from_itemidx=1&count=3&nolastread=1#wechat_redirect)、[#时间序列分析](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzUzNjk5Nzk1OA==&action=getalbum&album_id=1365472320852443141&scene=173&from_msgid=2247485344&from_itemidx=1&count=3&nolastread=1#wechat_redirect)   
机器学习领域，最新的时间序列预测领域论文。
- 时序人  [#时间序列](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3NDUwNTM3MA==&action=getalbum&album_id=1565545072782278657&scene=173&from_msgid=2247488450&from_itemidx=1&count=3&nolastread=1#wechat_redirect)、[#时间序列知识整理](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3NDUwNTM3MA==&action=getalbum&album_id=1588681516295979011&scene=173&from_msgid=2247484974&from_itemidx=1&count=3&nolastread=1#wechat_redirect)   
时间序列（Time Series）或简称为时序，是指将同一统计指标的数值按其发生的时间先后顺序排列而成的数列，  
无论在学术界还是工业、商业界，都有很广泛的研究与应用
- AIseer  [#时间序列](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIwNTU4ODMxMA==&action=getalbum&album_id=1956880417819508741&scene=173&from_msgid=2247485455&from_itemidx=1&count=3&nolastread=1#wechat_redirect)   
时间序列相关研究系列，包括单变量时序、多变量时序的异常检测、预测、聚类、相关分析等。
- kaggle竞赛宝典  [#时间序列](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzk0NDE5Nzg1Ng==&action=getalbum&album_id=1963949234009767937&scene=173&from_msgid=2247498373&from_itemidx=1&count=3&nolastread=1#wechat_redirect)
- AI蜗牛车  [#时空序列预测](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzA4ODUxNjUzMQ==&action=getalbum&album_id=1569913792686243845&scene=173&from_msgid=2247484711&from_itemidx=1&count=3&nolastread=1#wechat_redirect)、[#时间序列](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzA4ODUxNjUzMQ==&action=getalbum&album_id=1572765624546672644&scene=173&from_msgid=2247484945&from_itemidx=1&count=3&nolastread=1#wechat_redirect) 或 [#AI蜗牛车系列出品](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzA4ODUxNjUzMQ==&action=getalbum&album_id=1337863230869438464&scene=173&from_msgid=2247484363&from_itemidx=2&count=3&nolastread=1#wechat_redirect)（Ctrl+F：序列）  
时空序列预测研究笔记和代码实例~

---
jzh @ szu, ias

[^1]: 指导老师：王雄