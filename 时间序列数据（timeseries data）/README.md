# 混沌时间序列

考虑经典的 Lorenz 系统 (Lorenz-63) 以及与之对偶的 Chen 系统，作为范例. 

$$\text{Lorenz}\quad
\dot{x} = a (y-x),\enspace 
\dot{y} = c x - y - x z,\enspace 
\dot{z} = x y - b z. 
\qquad\text{Chen}\quad
\dot{x} = a (y-x),\enspace 
\dot{y} = (c - a)x + c y - x z,\enspace 
\dot{z} = x y - b z.
$$
其中 $a,b,c$ 是实参数. 
当左边的 $a = 10, b = 8/3, c = 28$ 时，Lorenz 系统处于混沌状态. 
当右边的 $a = 35, b = 3, c = 28$ 时，Chen 系统处于混沌状态. 

仿照大多数文献中的做法，对混沌系统作数值积分得到混沌时间序列. 

1. 常见的 ode **数值积分方法**有，四阶 Runge–Kutta 算法 (RK45) 和 Fortran ODEPACK 中的自动求解器 (LSODA). 
Python 中的 scipy.integrate.solve_ivp 实现了这两种算法. 
由于 Chen 系统的 RK45 解可能会收敛为极限环（这意味着误差过大），所以我们选择 LSODA. 
1. 文献中大多都是生成几万个数据点，然后舍掉前面一小部分的瞬态，余下的作为实验数据集. 
1. 对于时间序列数据集，一般是先划分训练集、验证集、测试集（如，$7:2:1$），然后再构建适用于监督学习模型的数据格式（ForecastDataset，
$\text{series to supervised}: [\text{timestamps}, \text{features}] \to [\text{samples}, \text{timesteps}, \text{features}]$，相空间重构 - 坐标延迟重构）. 
但也有一些研究中将这两个步骤反过来做，这样虽然可以有多一些样本，但在重构时间序列时就会遇到困难. 

参考  
[Information about Chen](http://www.ee.cityu.edu.hk/~gchen/chen-attractor.htm) - Generalized Lorenz Systems Family  
https://docs.scipy.org/doc/scipy/reference/integrate.html#solving-initial-value-problems-for-ode-systems

## 其他

除了上述两个，还有许多别的三维混沌系统，如 Rössler、Rabinovich–Fabrikant 系统.  
当然，也有更多变量的混沌时间序列，它们可以由 Lorenz-96 或 Kuramoto-Sivashinsky (K-S) 系统而得到. 
连续系统参考

1. [Serrano-Pérez2021, New Results for Prediction of Chaotic Systems Using Deep Recurrent Neural Networks](https://link.springer.com/article/10.1007%2Fs11063-021-10466-1) GRU-DRNN 多层循环神经网络
1. [Balachandran2019, Neural machine-based forecasting of chaotic dynamics](https://link.springer.com/article/10.1007%2Fs11071-019-05127-x) 多层 LSTM 的 seq2seq 模式，并带有“抑制器机制” (inhibitor mechanism). 
注：异步的序列到序列模式也称为编码器-解码器（Encoder-Decoder）模型

另外，也有一些离散的混沌系统，如一元的 Logistic 映射、二元的 Hénon 映射及其高维的推广. 
离散系统参考

1. [Sangiorgio2021, Forecasting of noisy chaotic systems with deep neural networks.](https://www.sciencedirect.com/science/article/pii/S0960077921009243) Observation noise & structural noise
1. [Sangiorgio2020, Robustness of LSTM neural networks for multi-step forecasting of chaotic time series.](https://www.sciencedirect.com/science/article/pii/S0960077920304422) Training LSTM without teacher forcing

# 从聚宽研究环境获取金融数据

- **get_price()** 股票、指数的 ***行情数据***（量价）[jq 历史行情数据](https://www.joinquant.com/help/data/stock#%E5%8E%86%E5%8F%B2%E8%A1%8C%E6%83%85%E6%95%B0%E6%8D%AE)
- **get_fundamentals()** 公司财务的 ***基本面数据***（市值、资产负债、现金流、利润）[jq 财务数据列表](https://www.joinquant.com/help/api/help#Stock:%E8%B4%A2%E5%8A%A1%E6%95%B0%E6%8D%AE%E5%88%97%E8%A1%A8)

量价和市值数据按交易日每天更新；  
而资产负债、现金流、利润，这三张财务数据表是按季度更新的，统计周期是一季度。  
其中由于资产负债表是存量性质的，查询其年度数据是返回第四季度的数据。

在聚宽研究环境中，除了 1G 的内存上限之外，貌似没有提取数据的限制。下面给出一个提取价格数据的范例，

```py
import datetime
# import numpy as np
import pandas as pd

## 先获取全市场所有股票的基本信息
df_stocks=get_all_securities(['stock'])
# 去掉退市的
df_stocks0 = df_stocks[df_stocks['end_date']==datetime.date(2200, 1, 1)]
# 保留长期上市的，不选新股
df_stocks1 = df_stocks0[df_stocks0['start_date']<datetime.date(2010, 1, 1)]
# 记录股票代码和名称
code1=list(df_stocks1.index) # len(code1) -> 1631
name1=list(df_stocks1['display_name'])
# 设置日期范围
start_date = '2010-01-01'
end_date = '2021-11-26'
## 直接一次提取，运行内存会超上限
# df_t = get_price(code1, start_date=start_date, end_date=end_date,
#                  frequency='1d', fields=['close'], panel=False)
## 按股票代码，逐个获取价格数据
price_data0 = pd.DataFrame()
for i in range(len(code1)):
    df_t = get_price(code1[i], start_date=start_date, end_date=end_date,
                     frequency='1d', fields=['close'], panel=False)
    df_t['code'] = code1[i]
    price_data0 = pd.concat([price_data0, df_t], sort=False)
price_data1=price_data0.reset_index().rename(columns={'index':'date'})
## 数据表透视，用 groupby 方法实现，得到一个更“干净的”数据表
# price_data2 = price_data1.pivot(index='date', columns='code')
price_data2 = pd.DataFrame()
for i in list(price_data1.groupby('code')):
    df_t1 = pd.DataFrame(i[1][['date','close']])\
    .rename(columns={'close':i[0]})\
    .set_index('date',drop=True)
    price_data2 = pd.concat([price_data2, df_t1], axis=1)
# 导出数据
price_data2.to_csv('price_data2.csv')
```

参考 [聚宽数据](https://www.joinquant.com/data) - 股票数据
