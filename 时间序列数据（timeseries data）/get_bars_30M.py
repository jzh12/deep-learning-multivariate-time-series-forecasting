import numpy as np
import pandas as pd
code_list = ['A8888.XDCE', 'AG8888.XSGE', 'AL8888.XSGE', 'AU8888.XSGE', 'BU8888.XSGE', 
             'C8888.XDCE', 'CF8888.XZCE', 'CS8888.XDCE', 'CU8888.XSGE', 'FG8888.XZCE', 
             'FU8888.XSGE', 'HC8888.XSGE', 'I8888.XDCE', 'IC8888.CCFX', 'IF8888.CCFX', 
             'IH8888.CCFX', 'J8888.XDCE', 'JD8888.XDCE', 'JM8888.XDCE', 'L8888.XDCE', 
             'M8888.XDCE', 'MA8888.XZCE', 'NI8888.XSGE', 'OI8888.XZCE', 'P8888.XDCE', 
             'PB8888.XSGE', 'PP8888.XDCE', 'RB8888.XSGE', 'RM8888.XZCE', 'RU8888.XSGE', 
             'SF8888.XZCE', 'SM8888.XZCE', 'SN8888.XSGE', 'SR8888.XZCE', 'T8888.CCFX', 
             'TA8888.XZCE', 'TF8888.CCFX', 'V8888.XDCE', 'Y8888.XDCE', 'SC8888.XINE', 
             'ZN8888.XSGE']

flds = ['high', 'open', 'low', 'close', 'volume', 'money'] # 
dfl = []
for fld in flds:
    df = get_bars(code_list, count=49000, unit='30m', end_dt='2024-03-29', 
                  fields=['date', fld], df=True)
    df0 = df.reset_index().rename(columns = {'date':'time','level_0':'code'})
    df1 = df0.pivot(index='time', columns='code')[fld]
    df2 = df1.dropna()
    dfl.append(df2)

df3 = pd.concat(dfl, axis=1, keys=flds)#, join='inner'
df4 = df3.stack()
df4.to_csv('ftr41_30M.csv') # 41 future
