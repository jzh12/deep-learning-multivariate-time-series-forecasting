# Evaluation procedure

<!--Once we have split the time series into training and test, we perform a series of preprocessing steps. 
Firstly, a normalization method is used to scale each time series of training data between 0 and 1, which helps to improve the convergence of deep neural networks. 
Then, we transform the time series into training instances to be fed to the models. There are several techniques available for this purpose: [95] 
the recursive strategy, which performs one-step predictions and feeds the result as the last input for the next prediction; 
the direct strategy, which builds one model for each time step; 
and the multi-output approach, which outputs the complete forecasting horizon vector using just one model. 
In this study, we have selected the Multi-Input Multi-Output (MIMO) strategy. 
Recent studies show that MIMO outperforms single-output approaches because, unlike the recursive strategy, it does not accumulate the error over the predictions. 
It is also more efficient in terms of computation time than the direct strategy since it uses one single model for every prediction. [95]
Following this approach, we use a moving-window scheme to transform the time series into training instances that can feed the models. 
This process slides a window of a fixed size, resulting in an input–output instance at each position. 
The deep learning models receive a window of fixed-length (past history) as input and have a dense layer as output with as many neurons as the forecasting horizon defined by the problem. 
The length of the input depends on the past-history parameter that should be decided. 
For the experimentation, we study three different values of past history depending on the forecast horizon (1.25, 2, or 3 times the forecast horizon). 
Figure 2 illustrates an example of this technique with 7 observations as past history and a forecasting horizon of 3 values.-->

一旦我们将时间序列拆分为训练和测试，我们将执行一系列预处理步骤。

首先，使用归一化方法将每个时间序列的训练数据缩放到 0 和 1 之间，
这有助于深度神经网络的收敛。

然后，我们将时间序列转换为训练实例以提供给模型。
有几种技术可用于此目的：[95]
1. 递归策略，它执行一步预测并将结果作为下一次预测的最后输入；
1. 直接策略，为每个时间步构建一个模型；
1. 多输出方法，它只使用一个模型输出完整的预测范围向量。

在本研究中，我们选择了多输入多输出 (MIMO) 策略。
最近的研究表明，MIMO 优于单输出方法，因为与递归策略不同，它不会累积预测的误差。
它在计算时间方面也比直接策略更有效，因为它为每个预测使用一个模型。 [95]

遵循这种方法，我们使用移动窗口方案将时间序列转换为可以提供模型的训练实例。
这个过程滑动一个固定大小的窗口，在每个位置产生一个输入-输出实例。

深度学习模型接收一个固定长度（过去历史）的窗口作为输入，并有一个密集层作为输出，其神经元数量与问题定义的预测范围一样多。
输入的长度取决于应确定的过去历史参数。
对于实验，我们根据预测范围（预测范围的 1.25、2 或 3 倍）研究过去历史的三个不同值。

参考：
- [arXiv:2103.12057](https://arxiv.org/abs/2103.12057) ***An Experimental Review on Deep Learning Architectures for Time Series Forecasting (Lara-Benitez2021)***
- [95] A review and comparison of strategies for multi-step ahead time series forecasting based on the NN5 forecasting competition（Taieb2012）


# 特征工程

归一化，二值化，离散化

这些预处理方法缩小了数据的变化范围，但没改变序列数据的“形态”。  
对于深度学习往往也是有效的：减低模型复杂度，

## 归一化

CNNs 适合用 z-score  
LSTM 适合用 min-max


## 离散化
信号处理中的量化编码

参考：
1. [小波分析 - 信号压缩] A Primer on WAVELETS and their Scientific Applications - 2.5 Quantization
1. [如何理解时间序列？— 从 Riemann 积分和 Lebesgue 积分谈起](https://zhuanlan.zhihu.com/p/34407471)
时间序列的 Lebesgue 表示，考虑时间序列的值分布情况（离散化、统计）
