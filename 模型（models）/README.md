# 时序模型

## 时序学习任务

对于时间序列，我们可以做什么？

1. [Python packages review (Siebert2021)](https://siebert-julien.github.io/time-series-analysis-python/)：
   - 分析任务（Analysis Tasks）  
     ：预测、分类、聚类、异常检测、分段、模式识别、突变点监测；
   - 数据处理（Data preparation）  
     ：降维、补缺、分解、预处理（归一化）、相似性度量；
   - 模型评估（Evaluation）  
     ：模型选择、超参寻优、特征选择、评价指标、统计检验、可视化；
2. [Sktime user guide (under development)](https://github.com/alan-turing-institute/sktime/issues/361)  
   ：变换、分类（预测类别目标变量）、回归（预测连续目标变量）、聚类、标注（异常检测）

我们的时序预测框架：数据集、模型库、评价指标、训练方法、超参优化、消融实验。  
主要考虑回归型的预测任务。

[^_^]: **回归**和**分类**这两种预测任务。

## 回归
把时间序列预测转化为监督学习问题，即考虑滑动窗口 $\left(x_{t-\ell+1}, \ldots, x_{t-1}, x_{t} \enspace ; \hspace{0.5em}  x_{t+1}, \ldots, x_{t+h}\right)$，其中 
- $\left(x_{t-\ell+1}, \ldots, x_{t-1}, x_{t}\right)$ 表示 “回看窗口”，$\ell$ 是 “滞后步长” (lag)，$\ell\in\mathbb{N}$；
- $\left(x_{t+1}, \ldots, x_{t+h}\right)$ 表示 “预测窗口”，$h$ 是 “预测步长” (horizon)，$h\in\mathbb{N}$. 
- 一般情况下，有 $h\le\ell$. 

$$\begin{aligned}
\textbf{时间序列}&\textbf{的自回归模型表达式}\newline
&\boxed{y = f(X)}
\end{aligned}$$
其中自变量 $X$ 是 “回看窗口”，因变量 $y$ 是 “预测窗口”，函数 $f$ 是时间序列模型。
1. 当 $h=1$ 时，$f$ 是 ***单步模型***；当 $h>1$ 时，$f$ 是 ***多步模型***。
1. 当滑动窗口的每个元素 $x_t$ 都是标量时，$f$ 表示 ***一元时间序列模型***；
    - 向量元素 $x_t^i$ 则对应了 ***多元模型***；
    - 张量元素 $x_t^{i1\cdots ir}$ 则对应了 ***多维模型***。
    - 这里我们考虑多元时序模型，其自变量个数为 $n$。
1. 某些神经网络模型可以处理 ***“可变长度” (Flexible history length)*** 的输入，即 “滞后步长” $\ell$ 不必是常数。
1. 特别的，若有作为输入而没输出的序列，也就是只取了 “预测窗口” 的部分序列作为因变量。  
即 $y = \gamma\circ f(X)$，其中 $\gamma:\left(\mathbb{R}^{h}\right)^{n} \to \left(\mathbb{R}^{h}\right)^{m},\quad m<n$ 表示某种[子集函数](https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/subset)。
则那些不在因变量里的被称为 ***“协变量” (Covariates)*** ，也叫 “外生变量”。
1. 另外，如果因变量 $y$ 不是预测窗口，而是预测窗口的某种类别标签。  
即对原来的因变量作一个变换 $g:\mathbb{R}^{n\times h} \to \mathbb{Z}^n$，得到 $y = g\circ f(X)$，这就是时间序列预测的 ***分类问题***。

---
- 某些神经网络模型支持 ***“多目标预测”*** ，这种 “多任务学习” 方式通过合并几个学习任务，可以提高模型的泛化能力。多目标预测类似于拉格朗日优化算法或KKT条件（拉格朗日乘数法？），也可以认为是一种带条件的 “正则化”。
- 因为时间序列连续变量的精确预测是几乎不可能的，所以有了上面的变换 $g$，将回归问题转化为分类问题。  
除此之外，也有 ***“不确定性” (Probabilistic、Uncertainty)*** 的改进。这给出了一定 **“准确率”** 下，预测结果的分布，而不是一个值。
- 虽然现在计算机的能力很强，但模型的 ***“计算需求” (Required computational resources)*** 须有一定的限度。  
因为工业化的模型是需要处理大量数据的，这时就得考虑模型的复杂度以及 **“效率”** 了。

## 模型列表

* FNN 多层全连接的前馈网络 aka MLP
  * [NBEATS](https://paperswithcode.com/paper/n-beats-neural-basis-expansion-analysis-for) (Element AI, 2019)【多层全连接, 残差连接】, arXiv:1905.10437
  * [NBEATSx](https://paperswithcode.com/paper/neural-basis-expansion-analysis-with) (cmu & nixtlats, 2021)【多元】, arXiv:2104.05522
  * [N-HiTS](https://paperswithcode.com/paper/n-hits-neural-hierarchical-interpolation-for) (cmu)【MLPs with ReLU nonlinearities】, arXiv:2201.12886 ⭐ ⭐ ⭐

* CNN 带有卷积层, (池化层) 的前馈网络
  * [TCN](https://paperswithcode.com/paper/an-empirical-evaluation-of-generic) (CMU Locus Lab, 2018)【dilated causal 卷积】, arXiv:1803.01271
  * Rocket, MiniRocket, [MultiRocket](https://paperswithcode.com/paper/multirocket-effective-summary-statistics-for) (Monash Uni)【卷积提取各种特征, 然后线性分类】, arXiv:2102.00457 ⭐ [TSC]
  * [SciNet](https://paperswithcode.com/paper/time-series-is-a-special-sequence-forecasting) (CUHK cure-lab, 2021)【降采样-卷积-相互作用】, arXiv:2106.09305
  * [TS2Vec](https://paperswithcode.com/paper/learning-timestamp-level-representations-for) (Peking Uni & Microsoft, 2021)【卷积，表示学习】, arXiv:2106.10466
  * [CoST](https://paperswithcode.com/paper/cost-contrastive-learning-of-disentangled-1) (SMU & Salesforce)【卷积，表示学习】, arXiv:2202.01575

* RNN 循环网络，更常见的是有门控机制改进的 LSTM 或 GRU
  * [LSTNet](https://paperswithcode.com/paper/modeling-long-and-short-term-temporal) (cmu, 2017)【CNN, RNN, skip 连接】, arXiv:1703.07015
  * [DeepAR](https://paperswithcode.com/paper/deepar-probabilistic-forecasting-with) (Amazon, 2017)【概率预测】, arXiv:1704.04110
  * [DeepState](https://paperswithcode.com/paper/deep-state-space-models-for-time-series) (Amazon, 2018)【状态空间】
  * [DeepGLO](https://paperswithcode.com/paper/think-globally-act-locally-a-deep-neural) (Amazon, 2019)【超级多元】, arXiv:1905.03806
  * [DeepVAR](https://paperswithcode.com/paper/high-dimensional-multivariate-forecasting)  (Amazon, 2019)【RNN, 高斯 copula 过程, mxnet】, arXiv:1910.03002
  * [AdaRNN](https://paperswithcode.com/paper/adarnn-adaptive-learning-and-forecasting-of) (Microsoft, 2021)【迁移学习】, arXiv:2108.04443
  * [HARNN](https://paperswithcode.com/paper/hyper-attention-recurrent-neural-network) (ncu, 2022)【迁移学习, no code】, arXiv:2202.10808

* Transformer 自注意力机制，‘编码-解码’架构的前馈网络
  * [Transformer](https://paperswithcode.com/paper/deep-transformer-models-for-time-series) (Google, 2020)【vanilla SA】, arXiv:2001.08317
  * [Reformer](https://paperswithcode.com/paper/reformer-the-efficient-transformer-1) (UC Berkeley & Google, 2020)【Locality sensitive hashing attention, Reversible】, arXiv:2001.04451
  * [TST](https://paperswithcode.com/paper/a-transformer-based-framework-for-1) (Brown Uni & IBM, 2020)【多元，表示学习】, arXiv:2010.02803
  * [Informer](https://paperswithcode.com/paper/informer-beyond-efficient-transformer-for) (Beihang Uni, 2020)【长序列，ProbSparse SA】, arXiv:2012.07436
  * [Autoformer](https://paperswithcode.com/paper/autoformer-decomposition-transformers-with) (Tsinghua Uni, 2021)【Auto-Correlation Mechanism】, arXiv:2106.13008
  * [Spacetimeformer](https://paperswithcode.com/paper/long-range-transformers-for-dynamic)(Uni Virginia, 2021)【有了 Transformer 就不需要 GNN】, arXiv:2109.12218 ⭐
  * [KLST](https://paperswithcode.com/paper/long-range-probabilistic-forecasting-in-time) (IIT Bombay, 2021) 【长程概率预测, 高阶统计量】, arXiv:2111.03394 ⭐
  * [ETSformer](https://paperswithcode.com/paper/etsformer-exponential-smoothing-transformers) (SMU & Salesforce)【指数平滑, code not yet】, arXiv:2202.01381
  * Preformer (Renmin Uni)【多尺度、逐段相关】, arXiv:2202.11356

* GNN 图网络，针对序列间关系的建模
  * ~~[DCRNN](https://paperswithcode.com/paper/diffusion-convolutional-recurrent-neural) (USC, 2017)【diffusion 卷积, seq2seq, scheduled 采样】, arXiv:1707.01926, 不适用于没有先验拓扑的情形~~
  * ~~[STGCN](https://paperswithcode.com/paper/spatio-temporal-graph-convolutional-networks) (Peking Uni, 2017)【ST-Conv, 图卷积, 图 Fourier 变换】, arXiv:1709.04875, 同上，只用来预测交通数据~~
  * [Graph-WaveNet](https://paperswithcode.com/paper/190600121) (Monash Uni, 2019)【图卷积, 自适应邻接矩阵; stacked dilated casual 卷积】, arXiv:1906.00121
  * [MTGNN](https://paperswithcode.com/paper/connecting-the-dots-multivariate-time-series) (Monash Uni, 2020)【残差连接, skip 连接, dilated inception】, arXiv:2005.11650
  * [StemGNN](https://paperswithcode.com/paper/spectral-temporal-graph-neural-network-for-1) (Peking Uni & Microsoft, 2021)【GFT & DFT, torch】, arXiv:2103.07719
  * [CauGNN](https://paperswithcode.com/paper/multivariate-time-series-forecasting-based-on) (CQU, 2021)【Granger Causality, no code】, arXiv:2005.01185
  * [IGMTF](https://paperswithcode.com/paper/instance-wise-graph-based-framework-for) (Sun Yat-sen Uni & Microsoft, 2021)【不同变量在不同时间的相关性】, arXiv:2109.06489
  * [SNAS4MTF](https://paperswithcode.com/paper/scale-aware-neural-architecture-search-for) (zju & Alibaba, 2021)【多尺度分解, 自适应图学习, 神经架构搜索, torch】, arXiv:2112.07459

  * [NRI](https://paperswithcode.com/paper/neural-relational-inference-for-interacting) (Uni Amsterdam, 2018)【variational 自编码器】, arXiv:1802.04687
  * [GTS](https://paperswithcode.com/paper/discrete-graph-structure-learning-for-1) (IBM, 2021)【循环图卷积, tf1】, arXiv:2101.06861
  * [CoGNN](https://paperswithcode.com/paper/online-multi-agent-forecasting-with) (sjtu, 2021)【在线学习, collaborative, code not yet】, arXiv:2107.00894

* 混合模型，魔改网络
  * [MQRNN](https://paperswithcode.com/paper/a-multi-horizon-quantile-recurrent-forecaster) (Amazon, 2017)【Seq2Seq, 多步概率预测】, arXiv:1711.11053
  * [DARNN](https://paperswithcode.com/paper/a-dual-stage-attention-based-recurrent-neural) (UCSD & NEC Labs, 2017)【LSTM, point-wise attention, 编码-解码】, arXiv:1704.02971
  * [MTNet](https://paperswithcode.com/paper/a-memory-network-based-solution-for) (NTU, 2018)【卷积-attention-GRU 编码器，多元】, arXiv:1809.02105
  * [TPA-LSTM](https://paperswithcode.com/paper/temporal-pattern-attention-for-multivariate) (NTU, 2018)【LSTM, Temporal Pattern Attention】, arXiv:1809.04206
  * [LogTrans](https://paperswithcode.com/paper/enhancing-the-locality-and-breaking-the) (UCSB, 2019)【causal 卷积 SA, LogSparse】, arXiv:1907.00235
  * [TFT](https://paperswithcode.com/paper/temporal-fusion-transformers-for) (Uni Oxford & Google, 2019)【GRN, LSTM, 多步概率预测, 协变量】, arXiv:1912.09363
  * [FC-GAGA](https://paperswithcode.com/paper/fc-gaga-fully-connected-gated-graph) (McGill Uni, 2020)【全连接门控图结构, tf】, arXiv:2007.15531
  * M-TCN (Wuhan Textile Uni, 2019)【multi-head TCN, tf keras (no code)】, doi:10.3390/electronics8080876
  * [MATCN](https://paperswithcode.com/paper/interpretable-multivariate-time-series) (Vrije Uni, 2020)【TCN, Attention, ML, 可解释, tf】, doi:10.1109/SSCI47803.2020.9308570
  * [DSANet](https://paperswithcode.com/paper/dsanet-dual-self-attention-network-for) (Westlake Uni, 2019)【卷积, SA, 多元, torch】, doi:10.1145/3357384.3358132

* 其他
  * [mWDN](https://paperswithcode.com/paper/multilevel-wavelet-decomposition-network-for) (Beihang Uni, 2018)【小波分解, 多频率 LSTM】, doi:10.1145/3219819.3220060
  * [Time2Vec](https://paperswithcode.com/paper/time2vec-learning-a-vector-representation-of) (Borealis AI, 2019)【表示学习, LSTM】, arXiv:1907.05321
  * [meta-learning](https://paperswithcode.com/paper/meta-learning-framework-with-applications-to) (Element AI, 2020)【残差连接：一种元学习适应机制】, arXiv:2002.02887
  * [TSER](https://paperswithcode.com/paper/time-series-regression) (Monash Uni, 2020)【将时序分类模型用于回归任务 -> Rocket】, arXiv:2006.12672
  * [RevIN](https://paperswithcode.com/paper/reversible-instance-normalization-for) (CUHK cure-lab, 2021)【正则化、迁移学习】

链接至 https://paperswithcode.com/

注：vanilla, 普通的；self-attention (SA)；

## 序列建模

1. 不像 RNN 需要 ***通过隐层节点序列往后传***，
1. 也不像 CNN 需要 ***通过增加网络深度来捕获远距离特征***，
1. Transformer 在这点上明显方案是相对简单直观的，***通过自注意（Self-attention）关联同一输入序列的不同位置***

注意力是如何发明的，各种注意力机制和模型 [Attention? Attention!](https://lilianweng.github.io/lil-log/2018/06/24/attention-attention.html)

Spacetimeformer 不需要 GNN [原来Transformer就是一种图神经网络，这个概念你清楚吗？ (qq.com)](https://mp.weixin.qq.com/s/DABEcNf1hHahlZFMttiT2g)
