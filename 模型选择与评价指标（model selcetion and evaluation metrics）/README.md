# 模型选择

参考 sklearn 用户指南 https://scikit-learn.org/stable/model_selection.html

## 交叉验证

通过不同地划分数据集，多次试验，确定较优参数。

1. 最常用交叉验证法是 $k$ 折交叉验证，先将数据集划分为 $k$ 个大小相似的互斥子集. 
1. 在训练时，选择其中 $k-1$ 个子集作为训练集，然后将余下的那个子集作为验证集，计算在余下子集的测试误差. 
1. 这样，我们有 $k$ 个训练/验证集，我们可以进行 $k$ 次训练和测试，得到 $k$ 个测试误差，最终返回平均测试误差，即这 $k$ 个测试误差的均值. 
1. 利用交叉验证进行模型选择就是选择平均测试误差最小的模型作为最优模型. 常用的 $k$ 的取值为 5、10、20 等. 

由于时间序列的序列关系限制，划分时序数据集时，需要避免用到未来的数据。

1. 首先将时间序列 $s$ 从后往前划分成 $k$ 个等长且尽可能长的序列，剩下的序列值作为初始训练数据 $x_0$. 
1. 即 $s = x_0 \cup x_1 \cup x_2 \cup \cdots \cup x_k,\enspace x_i \cap x_j = \emptyset$. 
1. 且 $x_0, x_1, x_2, \ldots, x_k$ 存在时间上的先后关系，将它们首尾相接就得到了序列 $s$. 
1. 第一次训练时，用 $x_0$ 作训练集，用 $x_1$ 作验证集；第二次训练时，用 $x_0 \cup x_1$ 作训练集，用 $x_2$ 作测试集. 
1. 依此类推，第 $k$ 次训练时，用 $x_0 \cup x_1 \cup x_2 \cup \cdots \cup x_{k−1}$ 作训练集，用 $x_k$ 作验证集. 
1. 我们仍然进行了 $k$ 次训练/测试，得到 $k$ 个测试误差，最后返回平均测试误差. 

参考  
https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html  
https://tscv.readthedocs.io/en/latest/index.html  
https://lonepatient.top/2018/06/10/time-series-nested-cross-validation

## 其他
神经网络模型的规模
https://www.quantamagazine.org/computer-scientists-prove-why-bigger-neural-networks-do-better-20220210/

# 模型评估
量化 “预测的质量（效果）”

## 分类模型的评价指标
参考 [11个模型评估指标(zhihu.com)](https://zhuanlan.zhihu.com/p/80437334)  
[精确率、召回率、F1 值、ROC、AUC 各自的优缺点是什么？(zhihu.com)](https://www.zhihu.com/question/30643044)

混淆矩阵 (Confusion Matrix) 也称误差矩阵，是表示精度评价的一种标准格式，用n行n列的矩阵形式来表示。  
对于二分类（Binary classification），n=2。

<table class="docutils align-default">
<tbody>
<tr class="row-odd"><td></td>
<td colspan="2"><p>Actual class (observation)</p></td>
</tr>
<tr class="row-even"><td rowspan="2"><p>Predicted class
(expectation)</p></td>
<td><p>tp (true positive)
Correct result</p></td>
<td><p>fp (false positive)
Unexpected result</p></td>
</tr>
<tr class="row-odd"><td><p>fn (false negative)
Missing result</p></td>
<td><p>tn (true negative)
Correct absence of result</p></td>
</tr>
</tbody>
</table>

- 准确率(Accuracy)：判断正确的，
- 精确率、查准率(Precision)：正例中判断正确的 $\text{precision} = \frac{tp}{tp + fp},$
- 真负率：负例中判断正确的，
- 召回率、查全率(Recall)、灵敏度(Sensitivity)：判断正确中的正例 $\text{recall} = \frac{tp}{tp + fn},$
- 特异度(Specificity)：判断正确中的负例

ROC 曲线是灵敏度和(1-特异度)之间的曲线。(1-特异性)也称为 ***假正率***，灵敏度也称为 ***真正率***。

AUC，即 area under the receiver operating characteristic (ROC) curve

## 回归模型的评价指标 or 损失函数
—— 长度与距离的定义，预测效果的量化（几何化）

- 绝对误差 (Absolute Error)，预测值与真实值之差的绝对值  
$AE = ABS\left(Actual_t - Forecast_t\right)$

    1. 平均绝对误差 (Mean Absolute Error/Deviation - MAE 或 MAD)，绝对误差的 ***算术平均***；
$$MAE = \frac{1}{N} \sum_{t=1}^N ABS\left(Actual_t - Forecast_t\right)$$
    1. 几何平均绝对误差 (Geometric Mean Absolute Error - GMAE)，绝对误差的 ***几何平均***；
$$\begin{aligned}
GMAE &= \exp\left(\frac{1}{N} \sum_{t=1}^N \log\left[ABS\left(Actual_t - Forecast_t\right)\right]\right)\newline
&= \left[\prod_{t=1}^N ABS\left(Actual_t - Forecast_t\right)\right]^{1/N}
\end{aligned}$$
    1. 根均方误差 (Root Mean Square Error - RMSE)，绝对误差的 ***平方平均*** MSE，再开根号；
$$RMSE = \sqrt{\frac{1}{N} \sum_{t=1}^N \left(Actual_t - Forecast_t\right)^2}$$

由均值不等式可知，GMAE ≤ MAE ≤ RMSE 对于任一组真实和预测序列都成立。  
这意味着它们三者中 RMSE 对异常值最敏感，GMAE 最不敏感，比较稳定。  
因而 GMAE 适合用来比较预测模型的优劣，但要求每项误差非零，且开 n 次根的计算耗时会更长。

- 绝对百分比误差 (Absolute Percentage Error)，绝对误差除以真实值，再乘以 100%  
$APE = \displaystyle\frac{ABS\left(Actual_t - Forecast_t\right)}{Actual_t}\times 100\\%$

     1. 平均绝对百分比误差 (Mean Absolute Percentage Error - MAPE)，绝对百分比误差的 ***算术平均***；
$$MAPE = \frac{1}{N} \sum_{t=1}^N \frac{ABS\left(Actual_t - Forecast_t\right)}{Actual_t}\times 100\\%$$
     1. 对称的MAPE (Symmetric Mean Absolute Percentage Error - SMAPE)，将 MAPE 中的分母 “真实值” 换为 “真实值和预测值两者的算术平均”；
$$SMAPE = \frac{1}{N} \sum_{t=1}^N \frac{ABS\left(Actual_t - Forecast_t\right)}{\left(Actual_t + Forecast_t\right)/2}\times 100\\%$$
     1. 反正切绝对百分比误差 (Mean Arctangent Absolute Percentage Error - MAAPE)，先用反正切函数把 APE 变换为一个有界量，再求算术平均；
$$MAAPE = \frac{1}{N} \sum_{t=1}^N \arctan\left[\frac{ABS\left(Actual_t - Forecast_t\right)}{Actual_t}\right]\times 100\\%$$
     1. 加权(平均)绝对百分比误差 (Weighted (Mean) Absolute Percentage Error - wMAPE 或 WAPE)，also called ***MAD/Mean Ratio***.
$$WAPE = \frac{\sum_{t=1}^N ABS\left(Actual_t - Forecast_t\right)}{\sum_{t=1}^N Actual_t}\times 100\\%$$

各种指标定义的汇总 [Statistical Forecast Errors (olivehorse.com)](https://blog.olivehorse.com/statistical-forecast-errors)  
几个误差指标的比较 [销量预测中的误差指标分析 - 曹明 - 博客园 (cnblogs.com)](https://www.cnblogs.com/think90/p/11883067.html)

## 解释性
除了定义，理解每个指标的 **几何意义** 或许更重要，就是所谓的 “解释性”。  
但是大多误差指标只能 “相对” 地来看。因为不知道它的几何意义，“绝对” 值也没法解释。

一种折中的方案就是把多步的预测值与真实值的走势画到同一张图上，用图形的直观作为辅助。

## 基于有效预测的模型评估
Performance Testing of Effective Forecasting

### 混沌时间序列0
- A data-driven, physics-informed framework for forecasting the spatiotemporal evolution of chaotic dynamics with nonlinearities modeled as exogenous forcings (Khodkar2021)  
    ⭐ Fig. 2. 两种预测方法的对比：走势图（Ground Truth and Prediction）、误差累计图（Error）、Lyapunov timescale $1/\Lambda_{\max}$，其中 $\Lambda_{\max}$ 为最大 Lyapunov 指数（leading Lyapunov exponent）. 
- Neural machine-based forecasting of chaotic dynamics (Balachandran2019)
    Lorenz63 系统、Lorenz96 系统、Kuramoto–Sivashinsk 系统；Lyapunov times $\Lambda_{\max} t$ 
- 深度学习预测混沌时间序列（熊有成2019）以预测的“有效时间”为预测效果的评价标准
- 长短期记忆网络预测混沌时间序列（熊有成2019）
    1. 在仿真实验中, 我们以 ***平均有效预测时间*** 作为度量指标, 使用模型对洛伦兹系统的状态变量进行预测, 并针对混沌系统特殊的动力学性质, 为模型搭配了四项策略来辅助预测. 
    1. 在训练阶段, 我们的目标是使模型输出尽可能地逼近真实的观测值。我们使用预测时间序列与真实观测值的均方误差(MSE)作为训练阶段的损失函数
- ⭐ 基于机器学习的混沌时间序列预测研究及应用（姜振豪2018）【多步、一元模型】
    1. 超参优化之网格搜索时，模型评价准则选取 ***均方误差***。  
    1. 对于多步预测，在模型比较时，可预测步数之前的测试误差是可以用来衡量模型性能的；而可预测步数之后的测试误差就不可靠了，有随机的成分在。完全可能存在这样的情况，模型在可预测步数之前性能非常好，远胜其他模型，但在可预测步数之后模型性能急剧恶化，远不如其他模型。那我们该如何评价该模型呢？显然，我们不应该关注可预测步数之后的性能，这是不可靠的，我们根本不应该在超出可预测步数，性能恶化的情况下继续预测。总的来说，对于多步预测模型，我们重点关注两个指标：***可预测步数***，以及 ***在可预测步数之前的测试误差***。
    1. 借鉴经验损失的思路，我们可以定义测试误差（test error）：模型 $f$ 关于测试集的平均损失 $\boxed{\displaystyle e_{test} = \frac1{m'}\sum_{i=1}^{m'} L\left(y_i, f(x_i)\right)}$
其中 $m'$ 为测试集中样本数目，$L$ 是损失函数（loss function），也常称为代价函数（cost function），用来度量错误程度的一个准则。损失函数是非负的，数值越小，错误
程度越小，模型预测就越好。训练误差反映的是模型对于未知数据的预测能力，因此我们可以用训练误差作为泛化误差的近似，来评价模型的泛化能力。

### 金融时间序列0
- 华泰金工【人工智能选股】模型评价：我们以 ***分层回测*** 的结果作为模型评价指标。我们还将给出 ***测试集的正确率、AUC*** 等衡量模型性能的指标。

参考 [人工智能研报(bigquant.com)](https://bigquant.com/wiki/doc/suanfa-PVUVCdX1Rp)

## 基于误差指标的模型评估
### 混沌时间序列1
- High-efficiency chaotic time series prediction based on time convolution neural network 基于TCN的混沌时间序列高效预测 (Jiagui Wu, 吴加贵2021)
***Evaluation index***
To measure the accuracy and stability of the model prediction,
this study uses four indicators: root mean square error (RMSE), mean absolute error (MAE), Spearman’s correlation coefficient ($\displaystyle\rho = 1-\frac{6\sum \left(Actual_t - Forecast_t\right)^2}{N\left(N^2 - 1\right)}$), and coefficient of determination ($\displaystyle R^2 = 1 - \frac{\sum \left(Actual_t - Forecast_t\right)^2}{\sum \left(Actual_t - ActualAverage\right)^2}$), 😱
- 基于深度差分神经进化的混沌时间序列预测研究（李永涛2021）在本文的预测实验中，使用平均绝对误差(MAE)、平均绝对百分比误差(MAPE)、均方根误差(RMSE)和均方根百分比误差(RMSPE)等四种标准来评价预测模型的性能
- 基于混合神经网络和注意力机制的混沌时间序列预测（李永涛2021）物理学报, 2021, 70(1): 010501. doi: 10.7498/aps.70.20200899
为了评价预测误差, 除了采用常用的均方根误差(RMSE)、平均绝对误差(MAE)和平均绝对百分比误差(MAPE)外, 再引入一个在评价时间序列预测性能中常用的均方根百分比误差(RMSPE), 
$$\begin{aligned}
MAE &= \frac{1}{N}\sum_{t = 1}^N \left\vert y_t - \hat y_t \right\vert, \qquad
RMSE &= \sqrt {\frac{1}{N}\sum_{t = 1}^N \left(y_t - \hat y_t\right)^2},\newline
MAPE &= \frac{1}{N}\sum_{t = 1}^N \frac{\left\vert\hat y_t - y_t \right\vert}{y_t} \times 100\\%, \qquad
RMSPE &= \sqrt {\frac{1}{N}\sum_{t = 1}^N \left| \frac{y_t - \hat y_t}{y_t} \right\vert^2} \times 100\\%
\end{aligned}$$
 😱
- 基于分数阶最大相关熵算法的混沌时间序列预测（王世元2018）【***自适应滤波器***，在 alpha 噪声环境下】为了评价滤波器的精度, 定义均方误差 (mean square error, MSE) 为
$\boxed{\displaystyle MSE(dB) = 10\log_{10}\frac1N\sum_{i=1}^N \left(d_i - y_i\right)^2}$
其中, $d_i$ 是滤波器的期望输出, $y_i$ 是滤波器的实际输出, $N = 100$ 是测试数据的个数. 
为了消除仿真中的随机性, 本文取 100 次蒙特卡罗仿真实验的平均值来计算 MSE.
- 混沌时间序列多步自适应预测方法（孟庆芳2006）【滤波器？多步自适应预测方法】以预测均方误差 $e_{\mathrm m}=MSE$ 和相对误差 $e_{\mathrm r}=MSE/SquaredMean$ 作为评测标准，仿真结果如图 1 所示（看不太清）
$$e_{\mathrm m} = \frac1N\sum_{k=1}^N \left\vert x(k)-\hat x(k)\right\vert^2, \qquad
e_{\mathrm r} = \frac{\sum_{k=1}^N \left[x(k)-\hat x(k)\right]^2}{\sum_{k=1}^N x^2(k)}$$

- 一种预测混沌时间序列的模糊神经网络方法（胡玉霞2005）仿真的结果为 $RMSE=0.0168$，最大相对误差为 $0.0289$，预测结果和真实值之间的比较如图2所示，相对误差结果如图3所示. 

一点疑惑：滤波器？滑动窗口？滚动训练？。。。

### 公开数据集
- 基于LSTM的时间序列混合预测方法研究（黄宏伟2018）【小波分解】  
本实验选择了四个客观评价标准：平均绝对误差（MAE）、均方误差（MSE）、均方根误差（RMSE）、平均绝对百分比误差（MAPE），其中 MAPE 可以消除不同数值范围对评价的影响，适用于不同数据集的横向对比。
- 神经网络及其组合模型在时间序列预测中的研究与应用（潘丽娜2018）【多步的预测策略、储备池计算（ESN）】  
    1. 对于准则的选择，目前还没有一个统一的标准，通常的做法都是使用多个准则从不同的角度来说明模型的性能。比较流行的误差准则有 MAE、MSE、RMSE、NMSE (Normalized MSE)、MAPE 和 IA (Index of Agreement)，
$$NMSE = \frac{MSE}{\sigma_y^2}, \qquad
IA = 1 - \frac{\sum_{t=1}^n \left(y_t - \hat y_t\right)^2}
{\sum_{t=1}^n \left(\vert y_t - \bar y\vert + \vert\hat y_t - \bar y\vert\right)^2}$$
此处，$y_t$ 代表第 $t$ 个真实值，$\hat y_t$ 代表第 $t$ 个预测值，$\bar y$ 表示真实值的平均，$\sigma_y^2$ 表示真实值的方差，$n$ 是真实值的数目。
    1. MSE 是一种广泛使用的误差准则，用来评估由模型产生的预测精度。由于采用平方计算的方式，MSE会惩罚较大的误差。
    1. NMSE 与衡量线性依赖关系的指标 $R^2$ (Coefficient of Determination)相关[133]，并且满足 $NMSE = 1 — R^2$。
    1. MAE、MSE 和 RMSE 都是依赖于数据尺度的评估方法。由于与统计建模理论相关，RMSE 和 MSE 在历史上曾备受欢迎，但是他们对于极端值的敏感度却比 MAE 要高[134]。
    1. MAPE 是一种相对的误差度量准则，它独立于数据尺度，对极端值不敏感，且对正误差的惩罚大于对负误差的惩罚。当时序数据都是正值且远大于零值时，MAPE是一种值得推荐的评估准则。
    1. 以上几种误差准则的值越小，表示模型的预测精度越高。
    1. IA 是一个无维度的指标，它的范围在 0 到 1 之间。IA = 0 意味着预测值与真实值是完全不一致的，而 IA = 1 则意味着预测值与真实值完全一致[135]。


## 模型评估小结

- 常见的方法是计算一些误差指标，然后也把所有模型在样本外预测的序列和真实序列画到同一张图上。既有数的度量，也有形的直观，可以大致刻画出不同模型在该时序数据上的预测 ***准确度***。
- 在金融数据上，模型的预测能力可以很好地得到检验。比如，若模型对各股量价数据的未来变化规律有一定的把握（概率意义上的），则在选股 ***回测中会有收益***。同一数据集，同一策略下，越接近市场变化规律本质的模型可获得越高收益。
- 混沌时间序列预测比金融市场更为简单，而且混沌系统具有 ***遍历性***，序列的任意一个片段可以在历史中找到相似的片段，所以可以有样本外短期的精准预测。
- 但另一方面，由于混沌系统具有 ***初值敏感性与非周期性***，所以在样本外长期时预测序列一定会偏离真实序列。
- 因此，我们考虑模型预测的 ***有效预测步长***（在一定误差范围内的样本外推步数）作为评价指标，该值越大表明模型的可以高精度逼近混沌系统的解。

